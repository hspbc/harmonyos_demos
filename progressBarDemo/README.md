# 什么是ProgressBar
ProgressBar是用于展示进度的UI控件,.
## 1.横向进度
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/d171cf9790ea4ec43707c1df1c62fed9.png)
### 代码
```xml
  <ProgressBar
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:max="100"
        ohos:min="0"
        ohos:progress="70"/>
```
## 2.纵向进度
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/529af5da5ced26d725a5e1decff32d72.png)
### 代码
```xml
<ProgressBar
        ohos:height="90vp"
        ohos:width="match_content"
        ohos:max="100"
        ohos:min="0"
        ohos:orientation="vertical"
        ohos:progress="70"
        ohos:progress_element="#FF8800"/>
```
## 3.横向分段展示进度
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/c21ddd1a242ef7c815ac5c23687444d6.png)
### 代码
```xml
<ProgressBar
        ohos:height="20vp"
        ohos:width="match_parent"
        ohos:divider_lines_enabled="true"
        ohos:divider_lines_number="5"
        ohos:max="100"
        ohos:min="0"
        ohos:progress="80"
        ohos:progress_element="#FF8800"
        ohos:progress_width="5vp"/>
```
## 4.横向进度条展示+文字
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/6b2060eef568c1d329e1c6a08b63116a.png)
### 代码
```xml
    <ProgressBar
        ohos:height="20vp"
        ohos:width="match_parent"
        ohos:max="100"
        ohos:min="0"
        ohos:progress="30"
        ohos:progress_element="#FF8800"
        ohos:progress_hint_text="30%"
        ohos:progress_hint_text_color="#000000"
        ohos:progress_width="5vp"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/progressBarDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:progress| 设置当前进度|
|ohos:max|设置最大进度值|
|ohos:min|设置最小进度值|
|ohos:orientation|设置展示方向，默认横向，可选值：vertical：纵向；horizontal：横向|
|ohos:progress_element|设置进度条颜色，样例：ohos:progress_element="#FF8800"|
|ohos:divider_lines_enabled|设置是否显示分段，可选值：true：显示分段；false：不显示|
|ohos:divider_lines_number|设置分段数量|
|ohos:progress_width|设置进度条的宽度，样例：ohos:progress_width="5vp"|
|ohos:progress_hint_text|设置进度条上文字，样例：ohos:progress_hint_text="30%"|
|ohos:progress_hint_text_color|设置进度条上文字颜色，样例：ohos:progress_hint_text_color="#000000"|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/3a1baf2918e8492ab61fa0600fb43444.png)