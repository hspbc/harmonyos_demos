package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice2 extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main2);

        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pager_slider);
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        Component component1 = layoutScatter.parse(ResourceTable.Layout_page1, null, false);
        Component component2 = layoutScatter.parse(ResourceTable.Layout_page2, null, false);

        List<Component> components = new ArrayList<>();
        components.add(component1);
        components.add(component2);
        pageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return components.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(components.get(i));
                return components.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(components.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });

        PageSliderIndicator pageSliderIndicator = (PageSliderIndicator) findComponentById(ResourceTable.Id_page_slider_indicator);
        pageSliderIndicator.setPageSlider(pageSlider);
    }
}