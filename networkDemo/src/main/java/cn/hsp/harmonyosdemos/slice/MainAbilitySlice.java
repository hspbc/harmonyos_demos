package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import cn.hsp.harmonyosdemos.network.ApiManager;
import cn.hsp.harmonyosdemos.network.Task;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_text).setClickedListener(component -> query());
    }

    private void query() {
        ApiManager.getInstance().getApiService().queryTask().enqueue(new Callback<Task>() {
            @Override
            public void onResponse(Call<Task> call, Response<Task> response) {
                if (!response.isSuccessful() || response.body() == null) {
                    onFailure(null, null);
                    return;
                }
                Task result = response.body();
                new ToastDialog(getContext()).setText(result.getName()).show();
            }

            @Override
            public void onFailure(Call<Task> call, Throwable throwable) {
                HiLog.warn(new HiLogLabel(HiLog.LOG_APP, 0, "===demo==="), "网页访问异常");
            }
        });
    }
}
