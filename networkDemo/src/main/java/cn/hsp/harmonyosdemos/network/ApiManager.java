package cn.hsp.harmonyosdemos.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class ApiManager {
    private static final String BASE_URL = "https://gitee.com/";

    private static ApiService apiService;
    private static ApiManager instance = new ApiManager();

    private ApiManager() {
        apiService = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build().create(ApiService.class);
    }

    public static ApiManager getInstance() {
        return instance;
    }

    public ApiService getApiService() {
        return apiService;
    }
}
