# 什么是DirectionalLayout
DirectionalLayout又称方向布局,是鸿蒙开发中几个常用的布局之一,使用频率较高,而且非常简单.布局内的控件依次排列,支持横向或纵向排列.
# 基础样例
## 1. 纵向排列
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/6355f43d3f0306206a760e3daf6d4047.png)
### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程1"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程2"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程3"/>
</DirectionalLayout>
```
代码说明:
1. 设置ohos:orientation为vertical,展示方向变成纵向
2. DirectionalLayout里面包括了三个显示文本的Text.
## 2. 横向排列
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/93753b902133173ac669a6f8366f2b22.png)
### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="horizontal">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程1"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程2"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程3"/>
</DirectionalLayout>
```
代码说明:
1. 设置ohos:orientation为horizontal,展示方向变成横向.
## 3. 调整子控件摆放位置(gravity属性)
通过DirectionalLayout的ohos:alignment属性控制其子控件相对于自己的对齐方式.
### 3.1 水平居中
#### 效果图
![](https://img-blog.csdnimg.cn/img_convert/0ff438da0d5607e2cf34bb73c8b639ee.png)
#### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="horizontal_center">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程-水平居中"/>
</DirectionalLayout>
```
### 3.2 垂直居中
#### 效果图
[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-y0eZrvJ6-1634824230834)(https://upload-images.jianshu.io/upload_images/6169789-11bb955a945217ce.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)]
#### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="vertical_center">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程-垂直居中"/>
</DirectionalLayout>
```
### 3.3 水平+垂直居中
#### 效果图
![](https://img-blog.csdnimg.cn/img_convert/c1864c9ad6621b584fdc4d720888d018.png)
#### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程-居中"/>
</DirectionalLayout>
````

## 4. 按比例分空间(layout_weight)
DirectionalLayout里的子控件可以通过weight属性按比例分空间大小(横向或纵向).按照DirectionalLayout里所有直属子控件(不算子控件的子控件)设置的weight作为总和,各个控件按照自己的weight所占总和比例来分空闲空间(有些控件未设置weight,则按照固定值).
### 4.1 等分空间
#### 效果图
![](https://img-blog.csdnimg.cn/img_convert/641d4e0a7da00243899f89865ee7b95e.png)
#### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="horizontal">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="#f2eada"
        ohos:text="Text1"
        ohos:text_alignment="center"
        ohos:weight="1"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="#9b95c9"
        ohos:text="Text2"
        ohos:text_alignment="center"
        ohos:weight="1"/>
</DirectionalLayout>
```
代码说明:
1.两个按钮都设置ohos:weight属性,且值相同,故平分空间.
### 4.2 按比例分
一个控件保持固定大小,一个占据剩余可用空间.
#### 效果图
![](https://img-blog.csdnimg.cn/img_convert/3d0ab412bb7055b80e197c0c25270607.png)
#### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="horizontal">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="#f2eada"
        ohos:text="Text1"
        ohos:text_alignment="center"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="#9b95c9"
        ohos:text="Text2"
        ohos:text_alignment="center"
        ohos:weight="1"/>
</DirectionalLayout>
```
代码说明:
1. 第一个按钮不设置ohos:weight属性,保持本身大小.
2. 第二个按钮设置ohos:weight属性,占据所有剩余可用空间.


## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/directionalLayoutDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右；top：居上；bottom：居下；horizontal_center：水平居中；vertical_center：垂直居中|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:orientation|子控件排列方向，可选值，horizontal：横向；vertical：纵向|
|weight|DirectionalLayout所有直属子控件,通过该属性值按比例分剩余可用空间|


# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)