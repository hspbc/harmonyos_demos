# 什么是RadioButton、RadioContainer
RadioButton和RadioContainer是用来显示一组单选框的,RadioButton就是单个单选框,RadioContainer就是这个组.
# 基础样例
## 1.展示一组单选框
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/10730e09f5ed4a8d29a81664f8064a2e.png)

### 代码
1）. 布局文件:activity_main.xml
```xml
<RadioContainer
    ohos:id="$+id:radioContainer"
    ohos:height="match_content"
    ohos:width="match_content">

    <RadioButton
        ohos:id="$+id:radioButton1"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:marked="true"
        ohos:text="我是RadioButton1"/>

    <RadioButton
        ohos:id="$+id:radioButton2"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="我是RadioButton2"/>
</RadioContainer>
```
2）. java代码:
```java
Text text = (Text) findComponentById(ResourceTable.Id_text);
RadioContainer container = (RadioContainer) findComponentById(ResourceTable.Id_radioContainer);
container.setMarkChangedListener((radioContainer, index) -> {
    text.setText("当前选中 : " + index);
});
```
完整代码:
```java
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Text text = (Text) findComponentById(ResourceTable.Id_text);
        RadioContainer container = (RadioContainer) findComponentById(ResourceTable.Id_radioContainer);

        container.setMarkChangedListener((radioContainer, index) -> {
            text.setText("当前选中 : " + index);
        });
    }
}
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/radioButtonDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:text | 设置文本内容 |
| ohos:text_size| 设置字号|
| ohos:text_color |设置颜色,样例：ohos:text_color="#FF0000",ohos:text_color="red"|
| ohos:italic| 设置是否斜体，可选值：true：斜体；false：正常字体|
|ohos:text_color_on|设置选中时，文字颜色，样例：ohos:text_color_on="#FF0000",|
|ohos:text_color_ff|设置选中时，文字颜色，样例：ohos:text_color_on="#00FF00",|
|ohos:marked|设置是否选中，可选值：true：选中；false：未选中|
更多属性及实际效果,可以在开发工具里自行体验.
![](https://img-blog.csdnimg.cn/img_convert/2fbd81fbb1224c299e87466d64bfb3e3.png)
