# 什么是DependentLayout
DependentLayout又称依赖布局,是鸿蒙开发中几个常用的布局之一,使用频率最高.支持相对于父控件或同级兄弟控件进行定位.
# 基础样例
## 1. 相对父控件定位
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/dc83ad4b65626144af5e020efaea1303.png)
### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="$media:beauty">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="#000000"
        ohos:padding="20vp"
        ohos:text="左上"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_end="true"
        ohos:background_element="#000000"
        ohos:padding="20vp"
        ohos:text="右上"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="#000000"
        ohos:center_in_parent="true"
        ohos:padding="20vp"
        ohos:text="正中间"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_bottom="true"
        ohos:background_element="#000000"
        ohos:padding="20vp"
        ohos:text="左下"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_bottom="true"
        ohos:align_parent_end="true"
        ohos:background_element="#000000"
        ohos:padding="20vp"
        ohos:text="右下"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>
</DependentLayout>
```
## 2. 相对同级兄弟控件对齐
设置本控件的一侧和目标控件(同级兄弟控件)的同侧对齐,如左侧对齐、右侧对齐.
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/c327f5c83e23de3bd041ec32ff72b51d.png)
### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="$media:beauty">

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_left="$id:center_text"
        ohos:background_element="#000000"
        ohos:text="左对齐"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_right="$id:center_text"
        ohos:background_element="#000000"
        ohos:text="右对齐"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:id="$+id:center_text"
        ohos:height="200vp"
        ohos:width="200vp"
        ohos:background_element="#80000000"
        ohos:center_in_parent="true"
        ohos:text="正中间"
        ohos:text_alignment="center"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_top="$id:center_text"
        ohos:background_element="#000000"
        ohos:text="上对齐"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_bottom="$id:center_text"
        ohos:background_element="#000000"
        ohos:text="下对齐"
        ohos:text_color="#FFFFFF"
        ohos:text_size="20fp"/>
</DependentLayout>
```
## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/dependentLayoutDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右；top：居上；bottom：居下；horizontal_center：水平居中；vertical_center：垂直居中|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)|
|ohos:above|当前控件整体位于目标控件之上.|
|ohos:below|当前控件整体位于目标控件之下.|
|ohos:align_left|和目标控件左对齐.|
|ohos:align_start|和目标控件左对齐.|
|ohos:align_right|和目标控件右对齐.|
|ohos:align_end|和目标控件右对齐.|
|ohos:align_top|和目标控件上对齐.|
|ohos:align_bottom|和目标控件下对齐.|
|ohos:center_in_parent|设置是否在父控件中居中(横向和纵向).|
|ohos:align_parent_left|设置是否和父控件左对齐.|
|ohos:align_parent_start|设置是否和父控件左对齐.|
|ohos:align_parent_right|设置是否和父控件右对齐.|
|ohos:align_parent_end|设置是否和父控件右对齐.|
|ohos:align_parent_top|设置是否和父控件上对齐.|
|ohos:align_parent_bottom|设置是否和父控件下对齐.|

更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)