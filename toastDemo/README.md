# 什么是Toast
Toast是用来短时显示提示信息并自动消失的UI控件. 默认一般显示在屏幕的下方.
# 基础样例
1. 显示默认toast
- 效果图:
![](https://img-blog.csdnimg.cn/img_convert/5bd3fcba117cd043879401ef56015469.gif)
- 代码:
```java
new ToastDialog(getContext()).setText("花生皮编程，哈哈").show();
```
2. 在顶部显示toast
- 效果图:
![](https://img-blog.csdnimg.cn/img_convert/9ba45f786debfb21d8285c568ea27376.gif)
- 代码:
```java
ToastDialog dialog = new ToastDialog(getContext());
dialog.setText("花生皮编程，哈哈");
dialog.setAlignment(LayoutAlignment.TOP);
dialog.show();
```
3. 居中显示toast
- 效果图:
![](https://img-blog.csdnimg.cn/img_convert/27b2fe5a65c86585e59f65adac1e8f76.gif)
- 代码:
```java
ToastDialog dialog = new ToastDialog(getContext());
dialog.setText("花生皮编程，哈哈");
dialog.setAlignment(LayoutAlignment.CENTER);
dialog.show();
```

## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/toastDemo

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)