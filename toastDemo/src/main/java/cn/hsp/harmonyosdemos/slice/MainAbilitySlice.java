package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Text text = (Text) findComponentById(ResourceTable.Id_text);
        text.setClickedListener(component -> new ToastDialog(getContext()).setText("花生皮编程，哈哈").show());

//        text.setClickedListener(component -> {
//            ToastDialog dialog = new ToastDialog(getContext());
//            dialog.setText("花生皮编程，哈哈");
//            dialog.setAlignment(LayoutAlignment.TOP);
//            dialog.show();
//        });

//        text.setClickedListener(component -> {
//            ToastDialog dialog = new ToastDialog(getContext());
//            dialog.setText("花生皮编程，哈哈");
//            dialog.setAlignment(LayoutAlignment.CENTER);
//            dialog.show();
//        });
    }
}