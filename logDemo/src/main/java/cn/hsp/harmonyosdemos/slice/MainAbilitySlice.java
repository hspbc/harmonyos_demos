package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, "[花生皮编程]");
        HiLog.debug(label, "打印调试信息");
        HiLog.info(label, "打印一般信息");
        HiLog.warn(label, "打印警告信息");
        HiLog.error(label, "打印错误信息");
        HiLog.fatal(label, "打印致命错误信息");
    }
}