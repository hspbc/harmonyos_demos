# 什么是HiLog
HiLog是鸿蒙程序用来打印日志的工具,相比于默认的控制台(`println`)输出,Log支持分级别过滤日志,方便按照关键字进行过滤等.
# 基础样例
1.  代码
```java
final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, "[花生皮编程]");
HiLog.debug(label, "打印调试信息");
HiLog.info(label, "打印一般信息");
HiLog.warn(label, "打印警告信息");
HiLog.error(label, "打印错误信息");
HiLog.fatal(label, "打印致命错误信息");
```
- HiLog.debug, 用于打印调试信息,这些信息用于在开发过程中确认问题,对应级别为debug,其显示优先级比verbose高一级.
- HiLog.info, 用于打印重要性高一些的信息,对应级别为info,其显示优先级比debug高一级.
- HiLog.warn, 用于打印警告信息,表示出现了需要引起重视的情况,对应级别为warn,其显示优先级比info高一级.
- HiLog.error, 用于打印错误信息,表示出现了严重的问题,应尽快修复,对应级别为error,其显示优先级比warn高一级.
- HiLog.fatal, 用于打印错误信息,表示出现了致命的问题,应立即修复,对应级别为fatal,其显示优先级最高.
2. 查看打印结果:
在Log窗口中查看打印结果.
Log主界面介绍:
![](https://img-blog.csdnimg.cn/img_convert/e4ab1c021b967c4d7ca8be113e6236bd.png)
- 区域1显示的是已连接的设备列表(含模拟器、真实手机),如果连接了多个设备,就从列表中选择正确的设备.
- 区域2用于选择过滤方式
![](https://img-blog.csdnimg.cn/img_convert/c20e4012f4c4e78d1e6b1343d5ed7387.png)
- “Show only selected application”表示只显示指定应用的日志.
- “No Filters”表示不做过滤.
- “Show only js log”表示只显示js日志.
- “Config Custom Filter”会打开一个新的界面用于定制新的过滤器.
![](https://img-blog.csdnimg.cn/img_convert/343535fa7091aa2a52b39a4398c386bc.png)
- 区域3显示的设备上运行的可调试的应用列表,如果区域2选择的是“Show only selected application”,那么就只会显示选中的应用的log日志.
- 区域4显示的是日志级别,级别越高显示的日志越少,级别越低显示的日志越多.级别:Verbose<Debug<Info<Warn<Error.
备注: 级别列表中还有一个Assert,从官方文档看,通过Log.wtf打印,是比Error级别更高的,但实测Log.wtf打印的也是Error级别.
- 区域5用于输入关键字过滤日志.
- 区域6用于标识区域4的输入信息是否为[正则表达式](https://www.runoob.com/regexp/regexp-syntax.html).

打印结果样例:
![](https://img-blog.csdnimg.cn/img_convert/7e696630487afcf9eaf5e80909fe06a3.png)

## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/logDemo

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)