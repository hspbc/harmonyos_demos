
# 什么是TabList
TabList一般用来实现标签栏.常见效果如下：  
![](https://img-blog.csdnimg.cn/img_convert/7098f6a11ccb3034a2d3810a6ed86d20.gif)
## 基本用法
1）布局文件代码：
```xml
    <TabList
        ohos:id="$+id:tab_list"
        ohos:height="50vp"
        ohos:width="match_parent"
        ohos:layout_alignment="center"
        ohos:normal_text_color="#000000"
        ohos:orientation="horizontal"
        ohos:selected_tab_indicator_color="#009ad6"
        ohos:selected_tab_indicator_height="2vp"
        ohos:selected_text_color="#009ad6"
        ohos:tab_length="60vp"
        ohos:tab_margin="20vp"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>
```
2）java代码：
```java
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Text text = (Text) findComponentById(ResourceTable.Id_text_title);

        TabList tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);

        TabList.Tab tab = tabList.new Tab(getContext());
        tab.setText("推荐");
        tabList.addTab(tab);

        TabList.Tab tab2 = tabList.new Tab(getContext());
        tab2.setText("视频");
        tabList.addTab(tab2);

        // 默认选中 某一个tab
        tab.select();

        // 设置监听
        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                // 当某个Tab从未选中状态变为选中状态时的回调
                text.setText(tab.getText()+"页");
            }

            @Override
            public void onUnselected(TabList.Tab tab) {
                // 当某个Tab从选中状态变为未选中状态时的回调
            }

            @Override
            public void onReselected(TabList.Tab tab) {
                // 当某个Tab已处于选中状态，再次被点击时的状态回调
            }
        });
    }
}
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/tabListDemo

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)