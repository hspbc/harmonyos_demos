# 什么是Image
Image是用于显示图片的UI控件.
# 基础样例
## 1.展示本地图片
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/b820dfbbdc275e46d98e4c9f39528b7e.png)
### 代码
```xml
<Image
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:scale_mode="clip_center"
        ohos:image_src="$media:beauty"/>
```
图片文件放在resources/base/media下
![](https://img-blog.csdnimg.cn/img_convert/17b6bb71ec14ef5d54db6cd9d1edab0f.png)
## 2.展示网络图片
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/3bd6f3f831f5a51301233a0dbd86364b.png)
### 代码
使用第三方开源库Glide加载网络图片
```java
Image image = (Image) findComponentById(ResourceTable.Id_image);
Uri uri =Uri.parse("https://c-ssl.duitang.com/uploads/blog/202105/21/20210521205533_613b9.jpg");
Glide.with(getContext()).load(uri).into(image);
```
完整代码:
- 1. build.gradle中添加依赖
```groovy
implementation 'io.openharmony.tpc.thirdlib:glide:1.1.2'
```
- 2. 完整加载图片代码
```java
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main_net);
        Image image = (Image) findComponentById(ResourceTable.Id_image);
        Uri uri =Uri.parse("https://c-ssl.duitang.com/uploads/blog/202105/21/20210521205533_613b9.jpg");
        Glide.with(getContext()).load(uri).into(image);
    }
}
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/imageDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:image_src|设置图片，样例：ohos:image_src="$media:icon"|
|ohos:scale_mode|设置图片缩放模式，可选值：zoom_center：居中缩放展示；zoom_start：居上缩放展示；zoom_end：居下缩放展示；center：居中展示(不缩放)；clip_center：居中裁剪；inside：左右拉伸铺满屏幕；stretch：上下拉伸铺满屏幕；|

更多属性及实际效果,可以在开发工具里自行体验.
![](https://img-blog.csdnimg.cn/img_convert/d485df942ba28d3ceaab7ce68a8ff391.png)

# 关于我
厦门大学计算机专业 | 前华为工程师  
专注分享编程技术，没啥深度，但是易懂。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)