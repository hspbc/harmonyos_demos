package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import com.bumptech.glide.Glide;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.utils.net.Uri;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 分享编程技术，没啥深度，但看得懂，适合初学者。
 * Java | 安卓 | 前端 | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 * 显示网络图片
 */
public class NetAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main_net);
        Image image = (Image) findComponentById(ResourceTable.Id_image);
        Uri uri =Uri.parse("https://c-ssl.duitang.com/uploads/blog/202105/21/20210521205533_613b9.jpg");
        Glide.with(getContext()).load(uri).into(image);
    }
}
