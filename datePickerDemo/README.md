
# 什么是Text
Text是用于展示文本内容的UI控件.常见效果如下：
![](https://img-blog.csdnimg.cn/img_convert/1c1faf98bfccf1a744d2a3e4dbdb39d0.png)
# 基础样例
## 1.普通文本
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/1ae9d3ea935839154c728f21b2a882d4.png)
### 代码
```xml
<Text
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:text="我是Text"/>
```
## 2.设置字号
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/81911d0b1f2aadd42b09284904ba7a2b.png)
### 代码
```xml
<Text
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:text="我是大号Text"
    ohos:text_size="60vp"/>
```
## 3.设置颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/af712edfc2058e83cdd3bb9d10f4171e.png)
### 代码
```xml
<Text
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:text="我是红色Text"
    ohos:text_color="#FF0000"/>
```
## 4.设置斜体
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/b2ee9363e8374d822897dbfb9f3dc924.png)
### 代码
```xml
<Text
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:text="我是斜体Text"
    ohos:italic="true"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/textDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:text_alignment|控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
| ohos:text | 设置文本内容 |
| ohos:text_size| 设置字号|
| ohos:italic| 设置是否斜体，可选值：true：斜体；false：正常字体|
| ohos:text_color |设置颜色,样例：ohos:text_color="#FF0000",ohos:text_color="red"|
| ohos:hint | 设置提示文本 |
| ohos:hint_color |设置颜色,样例：ohos:hint_color="#FF0000",ohos:hint_color="red"|
|ohos:multiple_lines|是否允许多行，可选值：true：允许；false：不允许|
|ohos:max_text_lines|最多显示的文本行数|
|ohos:element_top|设置上方图标，可选值：图片元素地址或色值，样例：ohos:element_top="#FF0000"，ohos:element_top="$media:icon"|
|ohos:element_bottom|设置下方图标，可选值：图片元素地址或色值，样例：ohos:element_bottom="#FF0000"，ohos:element_bottom="$media:icon"|
|ohos:element_left|设置左侧图标，可选值：图片元素地址或色值，样例：ohos:element_left="#FF0000"，ohos:element_left="$media:icon"|
|ohos:element_right|设置右侧图标，可选值：图片元素地址或色值，样例：ohos:element_right="#FF0000"，ohos:element_right="$media:icon"|
|ohos:element_start|设置起始方向图标，可选值：图片元素地址或色值，样例：ohos:element_start="#FF0000"，ohos:element_start="$media:icon"|
|ohos:element_end|设置结束方向图标，可选值：图片元素地址或色值，样例：ohos:element_end="#FF0000"，ohos:element_end="$media:icon"|
|ohos:text_font|设置字体，样例：ohos:text_font="sans-serif"，ohos:text_font="HwChinese-medium"|
|ohos:truncation_mode|文本超长时截断方式，可选值：ellipsis_at_end：尾部截断，显示三个点；ellipsis_at_middle：中间截断，显示三个点；ellipsis_at_start：头部截断，显示三个点；auto_scrolling：超长时滚动显示；如果不设置，则默认为自然截断，不显示三个点|
更多属性及实际效果,可以在开发工具里自行体验.
![](https://img-blog.csdnimg.cn/img_convert/4f99b5af768ab9f009b1771d733d25c7.png)
