# 什么是RoundProgressBar
RoundProgressBar是用于展示进度的圆形UI控件.  
![](https://img-blog.csdnimg.cn/img_convert/e84459c98dc2941c62721d00a76c503d.gif)

## 1.基本用法
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/fd8bda37fe4288fb688d5db9d0cbcd25.png)
### 代码
```xml
    <RoundProgressBar
        ohos:height="100vp"
        ohos:width="100vp"
        ohos:progress="20"/>
```
## 2.设置进度条颜色和粗细
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/46a7fc1b61d0e592fa894c881aa8e070.png)
### 代码
```xml
 <RoundProgressBar
        ohos:height="100vp"
        ohos:width="100vp"
        ohos:progress="20"
        ohos:progress_color="#47CC47"
        ohos:progress_width="10vp"/>
```
## 3.设置中间显示文字
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/3170b315ade7972d55c1b1112a48a092.png)
### 代码
```xml
<RoundProgressBar
        ohos:height="100vp"
        ohos:width="100vp"
        ohos:progress="20"
        ohos:progress_hint_text="圆形进度条"
        ohos:progress_hint_text_color="#007DFF"
        ohos:progress_hint_text_size="15fp"/>
```
## 4.设置弧形显示
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/44bc9a88be22d76f5ae8aa23dc3891e7.png)
### 代码
```xml
    <RoundProgressBar
        ohos:height="100vp"
        ohos:width="100vp"
        ohos:max_angle="270"
        ohos:progress="50"
        ohos:start_angle="45"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/roundProgressBarDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:progress| 设置当前进度|
|ohos:progress_color|设置进度条颜色，样例：ohos:progress_color="#FF8800"|
|ohos:progress_width|设置进度条的宽度，样例：ohos:progress_width="5vp"|
|ohos:progress_hint_text|设置进度条上文字，样例：ohos:progress_hint_text="30%"|
|ohos:progress_hint_text_color|设置进度条上文字颜色，样例：ohos:progress_hint_text_color="#000000"|
|ohos:progress_hint_text_size|设置进度条上文字字体大小，样例：ohos:progress_hint_text_size="15fp"|
|ohos:start_angle|设置弧形显示时，起始角度，样例：ohos:start_angle="45"|
|ohos:max_angle|设置弧形显示时，起始角度，样例：ohos:max_angle="270"|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/3a1baf2918e8492ab61fa0600fb43444.png)