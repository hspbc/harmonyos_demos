# 什么是ToggleButton
ToggleButton是一种用于显示开关状态的按钮控件，也Switch比较相似.
# 基础样例
## 1.普通开关
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/1986b3a17f85d43e44858dbfbcbba5f2.png)
### 代码
```xml
    <ToggleButton
        ohos:height="match_content"
        ohos:width="match_content"/>
```
## 2.选中开关
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/0c5ecac46c7203a11cf51dfb2444b231.png)

### 代码
```xml
<ToggleButton
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:marked="true"/>
```
## 3.设置文字
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/a68d72c84dc40f130507eb66d3654d30.png)
### 代码
```xml
 <ToggleButton
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text_state_off="关"
        ohos:text_state_on="开"/>
```
## 4.设置开关上文字颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/6d9e844cc0cc478e66785013daf8edd5.png)
### 代码
```xml
 <ToggleButton
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text_color_off="blue"
        ohos:text_color_on="red"
        ohos:text_state_off="关"
        ohos:text_state_on="开"/>
```

## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/toggleButtonDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
| ohos:text_size| 设置字号|
|ohos:text_color_on|设置选中时，文字颜色，样例：ohos:text_color_on="#FF0000",|
|ohos:text_color_off|设置未选中时，文字颜色，样例：ohos:text_color_off="#FF0000",|
|ohos:text_state_on|设置选中时文字，样例：ohos:text_state_on="ON"|
|ohos:text_state_off|设置未选中时文字，样例： ohos:text_state_off="OFF"|
|ohos:marked|设置是否选中，可选值：true：选中；false：未选中|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/3a1baf2918e8492ab61fa0600fb43444.png)