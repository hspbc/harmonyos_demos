# 什么是Switch
Switch是一种用于显示开关状态的UI控件.
# 基础样例
## 1.普通开关
### 效果图
![](https://upload-images.jianshu.io/upload_images/6169789-59a1a2c9b509a52b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/60)
### 代码
```xml
<Switch
        ohos:height="match_content"
        ohos:width="match_content"/>
```
## 2.选中开关
### 效果图
![](https://upload-images.jianshu.io/upload_images/6169789-782d47e0d9fe87e4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/60)
### 代码
```xml
<Switch
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:marked="true"/>
```
## 3.设置开关上文字
### 效果图
![](https://upload-images.jianshu.io/upload_images/6169789-f4b8519b439e6c58.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/60)
### 代码
```xml
    <Switch
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text_state_off="OFF"
        ohos:text_state_on="ON"/>
```
## 4.设置开关上文字颜色和字体大小
### 效果图
![](https://upload-images.jianshu.io/upload_images/6169789-04c9985b4be8bc8e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 代码
```xml
 <Switch
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text_color="red"
        ohos:text_size="15fp"
        ohos:text_state_off="OFF"
        ohos:text_state_on="ON"/>
```

## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/switchDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:text | 设置文本内容 |
| ohos:text_size| 设置字号|
|ohos:text_color|设置文字颜色，样例：ohos:text_color="#FF0000",|
|ohos:text_state_on|设置选中时文字，样例：ohos:text_state_on="ON"|
|ohos:text_state_off|设置未选中时文字，样例： ohos:text_state_off="OFF"|
|ohos:marked|设置是否选中，可选值：true：选中；false：未选中|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  