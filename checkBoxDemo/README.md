# 什么是CheckBox
CheckBox是用于显示复选框的UI控件.
# 基础样例
## 1.普通复选框
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/37d5c8cc8417a3e86f3870cfdf43e318.png)
### 代码
```xml
<Checkbox
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:text="我是Checkbox"/>
```
## 2.选中状态
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/62cc4424960d0fec7454dcba10b3c691.png)
### 代码
```xml
<Checkbox
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:marked="true"
    ohos:text="我是Checkbox"/>
```
## 3.设置字号
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/03a7567bdedee46f0b255fb8f5ec6f7c.png)
### 代码
```xml
<Checkbox
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:text="我是Checkbox"/>
```
## 4.设置颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/ec18000f9f354c1320c1ab93fdaadaeb.png)
### 代码
```xml
<Checkbox
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:text="我是Checkbox"
    ohos:text_color="#FF0000"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/checkBoxDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:text | 设置文本内容 |
| ohos:text_size| 设置字号|
| ohos:text_color |设置颜色,样例：ohos:text_color="#FF0000",ohos:text_color="red"|
| ohos:italic| 设置是否斜体，可选值：true：斜体；false：正常字体|
|ohos:text_color_on|设置选中时，文字颜色，样例：ohos:text_color_on="#FF0000",|
|ohos:text_color_ff|设置选中时，文字颜色，样例：ohos:text_color_on="#00FF00",|
|ohos:marked|设置是否选中，可选值：true：选中；false：未选中|

更多属性及实际效果,可以在开发工具里自行体验.