package cn.hsp.harmonyosdemos;

import ohos.agp.components.*;

import java.util.List;

public class MyProvider extends BaseItemProvider {
    private final List<String> dataList;

    public MyProvider(List<String> dataList) {
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public Object getItem(int i) {
        if (dataList != null && i >= 0 && i < dataList.size()) {
            return dataList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_list_container_item, null, false);
        } else {
            cpt = component;
        }
        String data = this.dataList.get(i);
        Text titleText = (Text) cpt.findComponentById(ResourceTable.Id_titleText);
        titleText.setText(data);
        return cpt;
    }
}