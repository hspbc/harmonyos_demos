package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.MyProvider;
import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TabList;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        MyProvider myProvider = new MyProvider(getData());
        listContainer.setItemProvider(myProvider);
    }

    private List<String> getData() {
        List<String> dataList = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            dataList.add("数据-" + (i));
        }
        return dataList;
    }
}