# 什么是运行时权限
为了更好的保护用户隐私及安全,鸿蒙引入运行时动态权限检查机制.应用需要访问一些涉及用户隐私及安全权限时,应用需要调用系统权限检查接口,查看是否已获得权限,如果没有,则由系统弹出权限申请窗口,由用户决定是否允许使用相应权限.
# 基础样例
下面例子中将申请读取存储权限并显示相册.
## 1. 声明权限
在config.json中增加如下声明：
```json
"reqPermissions": [
      {
        "name": "ohos.permission.READ_USER_STORAGE",
        "reason": "需要权限",
        "usedScene":
        {
          "ability": ["cn.hsp.harmonyosdemos.MainAbility"],
          "when": "always"
        }
      }
    ]
```
## 2. 在Ability代码中申请权限
```java
if (verifySelfPermission("ohos.permission.READ_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
    // 应用未被授予权限
    if (canRequestPermission("ohos.permission.READ_USER_STORAGE")) {
        // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
        requestPermissionsFromUser(
                new String[]{"ohos.permission.READ_USER_STORAGE"}, REQUEST_CODE_READ_USER_STORAGE);
    } else {
        // 显示应用需要权限的理由，提示用户进入设置授权
        new ToastDialog(getContext()).setText("请进入系统设置进行授权").show();
    }
}
```
添加获得申请权限后的逻辑处理：
```java
public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
    super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
    switch (requestCode) {
        case REQUEST_CODE_READ_USER_STORAGE: {
            if (grantResults.length > 0
                    && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                // 权限被授予之后做相应业务逻辑的处理
                mainAbilitySlice.showPhotos();
            } else {
                // 权限被拒绝
                new ToastDialog(getContext()).setText("权限被拒绝").show();
            }
        }
    }
}
```

## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/permissionDemo

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)