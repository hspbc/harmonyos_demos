package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.MainAbility;
import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.TableLayout;
import ohos.agp.components.Text;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */

public class MainAbilitySlice extends AbilitySlice {
    public static final int REQUEST_CODE_READ_USER_STORAGE = 0;   //自定义的一个权限请求识别码，用于处理权限回调
    private TableLayout tlAlbum;    //定义表格布局,用来加载图片控件
    private Text textLoading, textNum;  //定义正在加载文本，照片数量显示文本

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        MainAbility mainAbility = (MainAbility) getAbility();
        mainAbility.setMainAbilitySlice(this);
        initView();
        showPhotos();
    }

    private void initView() {
        //初始化相关UI组件
        tlAlbum = (TableLayout) findComponentById(ResourceTable.Id_tl_album);
        tlAlbum.setColumnCount(3);  //表格设置成3列
        textLoading = (Text) findComponentById(ResourceTable.Id_text_loading);
        textNum = (Text) findComponentById(ResourceTable.Id_text_num);
    }

    //定义加载显示图片的方法
    public void showPhotos() {
        //先移除之前的表格布局中的所有组件
        tlAlbum.removeAllComponents();
        //定义一个数组，用来存放图片的id，它的size就是照片数量
        ArrayList<Integer> img_ids = new ArrayList<Integer>();
        //初始化DataAbilityHelper，用来获取系统共享数据
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        try {
            //读取系统相册的数据
            ResultSet result = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, null, null);
            //根据获取的数据觉得“正在加载”提示是否显示
            if (result == null) {
                textLoading.setVisibility(Component.VISIBLE);
            } else {
                textLoading.setVisibility(Component.HIDE);
            }
            //遍历获取的数据，来动态加载表格布局中的图片组件
            while (result != null && result.goToNextRow()) {
                //从获取的数据中读取图片的id
                int mediaId = result.getInt(result.getColumnIndexForName(AVStorage.Images.Media.ID));
                //生成uri，后面会根据uri获取文件
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, "" + mediaId);
                //获取文件信息
                FileDescriptor filedesc = helper.openFile(uri, "r");
                //定义一个图片编码参数选项用于设置相关编码参数
                ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
                decodingOpts.desiredSize = new Size(300, 300);
                //根据文件信息生成pixelMap对象，该对象是设置Image组件的关键api
                ImageSource imageSource = ImageSource.create(filedesc, null);
                PixelMap pixelMap = imageSource.createThumbnailPixelmap(decodingOpts, true);
                //构造一个图片组件并且设置相关属性
                Image img = new Image(MainAbilitySlice.this);
                img.setId(mediaId);
                img.setHeight(300);
                img.setWidth(300);
                img.setMarginTop(20);
                img.setMarginLeft(20);
                img.setPixelMap(pixelMap);
                img.setScaleMode(Image.ScaleMode.ZOOM_CENTER);
                //在表格布局中加载图片组件
                tlAlbum.addComponent(img);
                img_ids.add(mediaId);
            }
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            e.printStackTrace();
        }
        //完成照片数量的刷新，如果没有照片，则在UI中显示“没有照片”的文本
        if (img_ids.size() > 0) {
            textLoading.setVisibility(Component.HIDE);
            textNum.setVisibility(Component.VISIBLE);
            textNum.setText("照片数量：" + img_ids.size());
        } else {
            textLoading.setVisibility(Component.VISIBLE);
            textLoading.setText("没有照片");
            textNum.setVisibility(Component.HIDE);
        }
    }

}
