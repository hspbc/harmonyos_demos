
# 什么是TextField
TextField是进行文本输入的UI控件.常见效果如下：
![](https://img-blog.csdnimg.cn/img_convert/07b5a763c4fe38f3a9f0cd8bcc438349.png)
# 基础样例
## 1.普通输入框
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/6da6235370daf2a892462718f31eec76.png)
### 代码
```xml
  <TextField
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="我是TextField"/>
```
## 2.设置字号
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/24328753da58faa55cf2e38ab41b5945.png)
### 代码
```xml
<TextField
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="我是大号TextField"
        ohos:text_size="30vp"/>
```
## 3.设置颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/aeec73d6a1f10f9e0f68728b755dd99b.png)
### 代码
```xml
<TextField
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="我是红色TextField"
        ohos:text_color="#FF0000"/>
```
## 4.设置斜体
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/c1f0249fc37de6658361858134c4a2c5.png)
### 代码
```xml
<TextField
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:italic="true"
        ohos:text="我是斜体TextField"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/textFieldDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:text_alignment|控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
| ohos:text | 设置文本内容 |
| ohos:text_size| 设置字号|
| ohos:italic| 设置是否斜体，可选值：true：斜体；false：正常字体|
| ohos:text_color |设置颜色,样例：ohos:text_color="#FF0000",ohos:text_color="red"|
| ohos:hint | 设置提示文本 |
| ohos:hint_color |设置颜色,样例：ohos:hint_color="#FF0000",ohos:hint_color="red"|
|ohos:multiple_lines|是否允许多行，可选值：true：允许；false：不允许|
|ohos:max_text_lines|最多显示的文本行数|
|ohos:element_top|设置上方图标，可选值：图片元素地址或色值，样例：ohos:element_top="#FF0000"，ohos:element_top="$media:icon"|
|ohos:element_bottom|设置下方图标，可选值：图片元素地址或色值，样例：ohos:element_bottom="#FF0000"，ohos:element_bottom="$media:icon"|
|ohos:element_left|设置左侧图标，可选值：图片元素地址或色值，样例：ohos:element_left="#FF0000"，ohos:element_left="$media:icon"|
|ohos:element_right|设置右侧图标，可选值：图片元素地址或色值，样例：ohos:element_right="#FF0000"，ohos:element_right="$media:icon"|
|ohos:element_start|设置起始方向图标，可选值：图片元素地址或色值，样例：ohos:element_start="#FF0000"，ohos:element_start="$media:icon"|
|ohos:element_end|设置结束方向图标，可选值：图片元素地址或色值，样例：ohos:element_end="#FF0000"，ohos:element_end="$media:icon"|
|ohos:text_font|设置字体，样例：ohos:text_font="sans-serif"，ohos:text_font="HwChinese-medium"|
|ohos:truncation_mode|文本超长时截断方式，可选值：ellipsis_at_end：尾部截断，显示三个点；ellipsis_at_middle：中间截断，显示三个点；ellipsis_at_start：头部截断，显示三个点；auto_scrolling：超长时滚动显示；如果不设置，则默认为自然截断，不显示三个点|
|ohos:element_cursor_bubble|设置输入时光标前面的气泡，样例：ohos:element_cursor_bubble="#FF0000"，ohos:element_cursor_bubble="$media:icon"|
|ohos:text_cursor_visible|设置输入时是否出现光标|

更多属性及实际效果,可以在开发工具里自行体验.
![](https://img-blog.csdnimg.cn/img_convert/344e8e3adcbb7c26ec18c48a93d325d4.png)
