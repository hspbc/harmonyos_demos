package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        PageFlipper pageSlider = (PageFlipper) findComponentById(ResourceTable.Id_page_flipper);

        for (int i = 1; i <= 100; i++) {
            Text text = new Text(this);
            text.setText("页面：" + i);
            text.setTextSize(100);
            pageSlider.addComponent(text);
        }
        pageSlider.startFlipping();
    }
}