# 什么是PositionLayout
PositionLayout又称准确位置布局,或绝对定位布局，子控件会指定精确的位置.
这种布局方式一般很少使用，因为屏幕大小千奇百怪，一般都不能指定绝对的位置，否则会很难看。
# 基础样例
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/429ac3df6d38a13db5f04bbb0d2f0c5b.png)
### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<PositionLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent">

    <Image
        ohos:height="200vp"
        ohos:width="200vp"
        ohos:image_src="$media:img1"
        ohos:position_x="30vp"
        ohos:position_y="40vp"
        ohos:scale_mode="clip_center"/>

    <Image
        ohos:height="200vp"
        ohos:width="200vp"
        ohos:image_src="$media:img2"
        ohos:position_x="80vp"
        ohos:position_y="260vp"
        ohos:scale_mode="clip_center"/>

    <Image
        ohos:height="200vp"
        ohos:width="200vp"
        ohos:image_src="$media:img3"
        ohos:position_x="120vp"
        ohos:position_y="480vp"
        ohos:scale_mode="clip_center"/>
</PositionLayout>
```

## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/positionLayoutDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右；top：居上；bottom：居下；horizontal_center：水平居中；vertical_center：垂直居中|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)|
|ohos:position_x|子控件用来设置横向坐标位置，样例：ohos:position_x="30vp"|
|ohos:position_y|子控件用来设置纵向坐标位置，样例：ohos:position_y="30vp"|

更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)