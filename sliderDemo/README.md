# 什么是Slider
Slider是让用户调节进度的UI控件.常见效果如下：  
![](https://img-blog.csdnimg.cn/img_convert/0b2d22fb1a823fb971f446fc21847613.gif)

## 1.基本用法
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/808db24ea98e3665110c8e2d4d1692cc.png)
### 代码
```xml
<Slider
        ohos:id="$+id:slider"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:max="100"
        ohos:min="0"
        ohos:orientation="horizontal"
        ohos:progress="30"/>
```
## 2.设置进度条颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/c6faf83e4fb80e4aaea19d29e53aea61.png)
### 代码
```xml
<Slider
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:max="100"
        ohos:min="0"
        ohos:orientation="horizontal"
        ohos:progress="80"
        ohos:progress_color="#f47920"/>
```
## 3.设置进度条上文字
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/6973c61c3fde717914ed38076f1152d4.png)
### 代码
```xml
    <Slider
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:max="100"
        ohos:min="0"
        ohos:orientation="horizontal"
        ohos:progress_hint_text="当前进度"
        ohos:progress_hint_text_color="#000000"
        ohos:progress="80"
        ohos:progress_color="#f47920"/>
```
## 4.设置进度条文字位置
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/1992823f7cd7b2a34ca2460a13ebf1e6.png)
### 代码
```xml
  <Slider
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:max="100"
        ohos:min="0"
        ohos:orientation="horizontal"
        ohos:progress_hint_text="当前进度"
        ohos:progress_hint_text_color="#f47920"
        ohos:progress_hint_text_alignment="top|horizontal_center"
        ohos:progress_hint_text_size="30fp"
        ohos:progress="80"
        ohos:progress_color="#f47920"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/sliderDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:progress| 设置当前进度|
|ohos:max|设置最大进度值|
|ohos:min|设置最小进度值|
|ohos:orientation|设置展示方向，默认横向，可选值：vertical：纵向；horizontal：横向|
|ohos:progress_color|设置进度条颜色，样例：ohos:progress_color="#FF8800"|
|ohos:progress_width|设置进度条的宽度，样例：ohos:progress_width="5vp"|
|ohos:progress_hint_text|设置进度条上文字，样例：ohos:progress_hint_text="30%"|
|ohos:progress_hint_text_color|设置进度条上文字颜色，样例：ohos:progress_hint_text_color="#000000"|
|ohos:progress_hint_text_size|设置进度条上文字大小，样例：ohos:progress_hint_text_size="30fp"|
|ohos:progress_hint_text_alignment|设置进度条上文字位置，可选值：top：上方；bottom：下方；left：左边；right：右边；horizontal_center：水平居中；vertical_center：垂直居中；center：居中；start：和开始方向一致；end：和结束方向一致；样例：ohos:progress_hint_text_alignment="top|horizontal_center"|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/3a1baf2918e8492ab61fa0600fb43444.png)