package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_text1).setClickedListener(component ->
                {
                    Intent newIntent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withBundleName("cn.hsp.harmonyosdemos")
                            .withAbilityName("cn.hsp.harmonyosdemos.MainAbility2")
                            .build();
                    newIntent.setOperation(operation);
                    newIntent.setParam("name","花生皮编程");
                    newIntent.setParam("school","厦门大学");
                    newIntent.setParam("major","计算机科学与技术");
                    startAbility(newIntent);
                }
        );
    }
}
