# 什么是Ability
Ability是鸿蒙应用提供能力的一种抽象。Ability又分为：FA（Feature Ability）和PA（Particle Ability）两种。Feature Ability支持Page Ability，那什么是Page Ability呢？
其实就是你所看到的各个界面,每个界面都是一个Page Ability.如果把整个app比作一个商场,那Page Ability就是每一个店面,里面可以用来陈列各种商品.
# 认识默认创建的Ability类
![](https://img-blog.csdnimg.cn/img_convert/57becb2b303492bd10e2807b44043016.png)
1. ` super.onStart(intent);` : 调用父类的onStart，默认写法,也必须调用
2. `super.setMainRoute(MainAbilitySlice.class.getName());`: 加载Slice类,界面显示的主体在Slice类中.
# 认识默认创建的Slice类
![](https://img-blog.csdnimg.cn/img_convert/0aec75e10e13f93c7e7def8787f3452f.png)
1. ` super.onStart(intent);` : 调用父类的onStart，默认写法,也必须调用
2. `super.setUIContent(ResourceTable.Layout_ability_main);`: 加载布局文件,“布局文件”相当于是设计稿,加载的动作相当于把设计稿变成现实.
# Ability注册
所有Ability都必须在config.json文件中注册(该文件位于entry/src/main目录下).
![](https://img-blog.csdnimg.cn/img_convert/13e7a88d331044319eda64ac12459aa3.png)
```json
{
  "app": {
    "bundleName": "cn.hsp.myapplication.hmservice",
    "vendor": "hsp",
    "version": {
      "code": 1000000,
      "name": "1.0.0"
    }
  },
  "deviceConfig": {},
  "module": {
    "package": "cn.hsp.myapplication",
    "name": ".MyApplication",
    "mainAbility": "cn.hsp.myapplication.MainAbility",
    "deviceType": [
      "phone",
      "tablet",
      "tv",
      "wearable",
      "car"
    ],
    "distro": {
      "deliveryWithInstall": true,
      "moduleName": "entry",
      "moduleType": "entry",
      "installationFree": true
    },
    "abilities": [
      {
        "skills": [
          {
            "entities": [
              "entity.system.home"
            ],
            "actions": [
              "action.system.home"
            ]
          }
        ],
        "orientation": "unspecified",
        "visible": true,
        "name": "cn.hsp.myapplication.MainAbility",
        "icon": "$media:icon",
        "description": "$string:mainability_description",
        "label": "$string:entry_MainAbility",
        "type": "page",
        "launchType": "standard"
      }
    ]
  }
}
```
上述代码中abilities节点就是用来注册Ability的.
每个App都需要有一个启动入口，这个通过mainAbility来指定。
```json
"mainAbility": "cn.hsp.myapplication.MainAbility"
```
# Slice对应的布局文件ability_main.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:orientation="vertical">

    <Text
        ohos:id="$+id:text_helloworld"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$graphic:background_ability_main"
        ohos:layout_alignment="horizontal_center"
        ohos:text="$string:mainability_HelloWorld"
        ohos:text_size="40vp"
        />

</DirectionalLayout>
```
![](https://img-blog.csdnimg.cn/img_convert/bd5f683b24af5c7e5bf6f34147c4c828.png)
# 给界面对应的布局文件添加更多视图
1. 添加一个文字控件(Text)
```xml
<Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程"
        ohos:text_size="40vp"/>
```
完整代码如下:
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:orientation="vertical">

    <Text
        ohos:id="$+id:text_helloworld"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$graphic:background_ability_main"
        ohos:layout_alignment="horizontal_center"
        ohos:text="$string:mainability_HelloWorld"
        ohos:text_size="40vp"
        />
    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程"
        ohos:text_size="40vp"/>

</DirectionalLayout>
```
效果图:
![](https://img-blog.csdnimg.cn/img_convert/b67de08077e89cea36b4f7ba3339557e.png)
2. 改变布局方式,从纵向排列改成横向排列
修改DirectionalLayout中ohos:orientation属性值为`horizontal`,完整如下:
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="horizontal">

    <Text
        ohos:id="$+id:text_helloworld"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$graphic:background_ability_main"
        ohos:text="$string:mainability_HelloWorld"
        ohos:text_size="30vp"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程"
        ohos:text_size="30vp"/>

</DirectionalLayout>
```
效果图:
![](https://img-blog.csdnimg.cn/img_convert/0b855043e30038e227f546268dd97212.png)
# 在java代码里操控布局文件里的控件
1. 在此之前,需要给每个需要操控的控件设置一个唯一的id
通过`ohos:id`指定,格式为:`ohos:id="$+id:自定义控件ID"`,样例如:`ohos:id="$+id:text1"`
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:orientation="vertical">

    <Text
        ohos:id="$+id:text1"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="欢迎，欢迎"
        ohos:text_size="30vp"/>

    <Text
        ohos:id="$+id:text2"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="花生皮编程"
        ohos:text_size="30vp"/>

</DirectionalLayout>
```
2. 在java代码里给按钮控件添加点击事件
下面代码用于给按钮增加点击事件,`new ToastDialog(getContext()).setText("哈哈1").show());`用于显示一个短暂的提示消息.
```java
findComponentById(ResourceTable.Id_text1).setClickedListener(component -> new ToastDialog(getContext()).setText("哈哈1").show());
```
完整代码:
```java
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_text1).setClickedListener(component -> new ToastDialog(getContext()).setText("哈哈1").show());
        findComponentById(ResourceTable.Id_text2).setClickedListener(component -> new ToastDialog(getContext()).setText("哈哈2").show());
    }
}

```
效果图:
![](https://img-blog.csdnimg.cn/img_convert/906bc1717b4ac1803b352e453a5733ff.gif)

# 手动创建Ability
1. 在工程目录中单击右键,选择“新建>Ability>Empty Page Ability(Java)”
![](https://img-blog.csdnimg.cn/img_convert/f9d4695ed546fc330781cda8601d2e15.png)
2. 在窗口中设置Ability名称,对应布局文件名称.
![](https://img-blog.csdnimg.cn/img_convert/0127f7e97d928405b12827c1cbb92f5d.png)
3. 创建后的代码工程截图如下
![](https://img-blog.csdnimg.cn/img_convert/3600bed17acfebda7801770be35a765e.png)
4. 修改对应布局文件:ability_main2.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:orientation="vertical">

    <Text
        ohos:id="$+id:text_helloworld"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$graphic:background_ability_main2"
        ohos:layout_alignment="horizontal_center"
        ohos:text="第二个页面"
        ohos:text_size="40vp"
        />

</DirectionalLayout>
```
5. 修改一下第一个页面：ability_main.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:orientation="vertical">

    <Text
        ohos:id="$+id:text1"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="跳转"
        ohos:text_size="30vp"/>

</DirectionalLayout>
```
![](https://img-blog.csdnimg.cn/img_convert/1190a5ffbac3cd16a1d073ad88d1bd85.png)

# Ability跳转
下面演示点击“跳转”按钮时,从MainAbility跳转到Main2Ability：
```java
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_text1).setClickedListener(component ->
                {
                    Intent newIntent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withBundleName("cn.hsp.harmonyosdemos")
                            .withAbilityName("cn.hsp.harmonyosdemos.MainAbility2")
                            .build();
                    newIntent.setOperation(operation);
                    startAbility(newIntent);
                }
        );
    }
}
```
效果图:
![](https://img-blog.csdnimg.cn/img_convert/53c246bca662d009e85639f9853dde81.gif)
# Ability带数据跳转
1. 源Ability发送数据
通过调用intent的setParam方法传递数据
```java
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_text1).setClickedListener(component ->
                {
                    Intent newIntent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withBundleName("cn.hsp.harmonyosdemos")
                            .withAbilityName("cn.hsp.harmonyosdemos.MainAbility2")
                            .build();
                    newIntent.setOperation(operation);
                    newIntent.setParam("name","花生皮编程");
                    newIntent.setParam("school","厦门大学");
                    newIntent.setParam("major","计算机科学与技术");
                    startAbility(newIntent);
                }
        );
    }
}
```
2. 目标Ability接收数据
```java
        String name = intent.getStringParam("name");
        String school = intent.getStringParam("school");
        String major = intent.getStringParam("major");
```
a) 修改一下目标界面,将数据展示出来.
- 修改ability_main2.xml,添加显示数据的控件
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:padding="10vp"
    ohos:orientation="vertical">

    <Text
        ohos:id="$+id:text2"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$graphic:background_ability_main2"
        ohos:layout_alignment="horizontal_center"
        ohos:multiple_lines="true"
        ohos:text="第二个页面"
        ohos:text_size="30vp"/>

</DirectionalLayout>
```
b) 显示数据
接收及显示数据的完整代码:
```
public class MainAbility2Slice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main2);

        String name = intent.getStringParam("name");
        String school = intent.getStringParam("school");
        String major = intent.getStringParam("major");
        ((Text) findComponentById(ResourceTable.Id_text2)).setText("姓名：" + name + "\n学校：" + school + "\n专业：" + major);

    }
}
```
3. 效果图
![](https://img-blog.csdnimg.cn/img_convert/2567225fe5d983e66834992a3e8e8c3a.gif)

## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/pageAbilityDemo

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)