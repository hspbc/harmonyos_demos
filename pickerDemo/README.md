# 什么是Picker
Picker是一种滚轮式选择器.常见效果如下：  
![](https://img-blog.csdnimg.cn/img_convert/3945c08e426983cd41e36717b0ce5a59.gif)

## 1.基本用法
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/317fc598e8a8c8e2fa9856e47105b39b.png)
### 代码
```xml
 <Picker
        ohos:height="120vp"
        ohos:width="match_content"
        ohos:max_value="100"
        ohos:min_value="0"/>
```
## 2.设置背景
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/583ec1bce56b8632dd9084c024736d1c.png)
### 代码
```xml
<Picker
        ohos:height="120vp"
        ohos:width="match_content"
        ohos:max_value="100"
        ohos:background_element="#d3d7d4"
        ohos:min_value="0"/>
```
## 3.设置文字字体大小和颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/07d8201610ee06521b5c1a982fd80d3d.png)
### 代码
```xml
    <Picker
        ohos:id="$+id:picker"
        ohos:height="120vp"
        ohos:width="match_content"
        ohos:background_element="#d3d7d4"
        ohos:max_value="100"
        ohos:min_value="0"
        ohos:normal_text_color="#0c212b"
        ohos:normal_text_size="16fp"
        ohos:selected_text_color="#ef5b9c"
        ohos:selected_text_size="20fp"
        ohos:wheel_mode_enabled="true"/>
```
## 4.设置分割条颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/b851ccabd0b8ae5da0d73ae0e1d99c42.png)
### 代码
```xml
<Picker
        ohos:height="120vp"
        ohos:width="match_content"
        ohos:background_element="#d3d7d4"
        ohos:max_value="100"
        ohos:min_value="0"
        ohos:normal_text_color="#0c212b"
        ohos:normal_text_size="16fp"
        ohos:selected_text_color="#ef5b9c"
        ohos:selected_text_size="20fp"
        ohos:bottom_line_element="red"
        ohos:top_line_element="blue"
        ohos:wheel_mode_enabled="true"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/pickerDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:max_value|设置最大值，样例：ohos:max_value="100"|
|ohos:min_value|设置最小值，样例：ohos:min_value="0"|
|ohos:normal_text_color|设置非选中文字颜色，样例：ohos:normal_text_color="#000000"|
|ohos:normal_text_size|设置非选中文字大小，样例：ohos:normal_text_size="30fp"|
|ohos:selected_text_color|设置选中文字颜色，样例：ohos:selected_text_color="#000000"|
|ohos:selected_text_size|设置选中文字大小，样例：ohos:selected_text_size="30fp"|
|ohos:top_line_element|设置上分割线颜色，样例：ohos:top_line_element="#000000"|
|ohos:bottom_line_element|设置下分割线颜色，样例：ohos:bottom_line_element="#000000"|
|ohos:wheel_mode_enabled|列表是否循环，可选值：true：循环；false：不循环，样例：ohos:wheel_mode_enabled="false"|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/3a1baf2918e8492ab61fa0600fb43444.png)