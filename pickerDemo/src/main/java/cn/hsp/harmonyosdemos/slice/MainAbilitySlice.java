package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Picker;
import ohos.agp.components.Slider;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Picker picker1 = (Picker)findComponentById(ResourceTable.Id_picker1);
        picker1.setDisplayedData(new String[]{"星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"});

        Picker picker2 = (Picker)findComponentById(ResourceTable.Id_picker2);
        List<String> hours = new ArrayList<>();
        for(int i=1;i<=12;i++){
            hours.add(i+"时");
        }
        picker2.setMaxValue(12);
        picker2.setDisplayedData(hours.toArray( new String[]{}));

        Picker picker3 = (Picker)findComponentById(ResourceTable.Id_picker3);

        List<String> minutes = new ArrayList<>();
        for(int i=0;i<=59;i++){
            minutes.add(i+"分");
        }
        picker3.setMaxValue(60);
        picker3.setDisplayedData(minutes.toArray( new String[]{}));
    }
}