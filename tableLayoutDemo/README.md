# 什么是TableLayout
TableLayout又称表格布局,用于以表格形式展示内容. 
## 1. 样例：2*2表格
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/ffe99a0b5d7056b861a6132dcb189df4.png)
### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<TableLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="$media:beauty"
    ohos:column_count="2"
    ohos:padding="8vp"
    ohos:row_count="2">

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="1"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="2"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="3"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="4"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>
</TableLayout>
```
## 2. 样例：3*3表格
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/cbfaad6c94b3ae7409c4d76ff2f60fe2.png)
### 代码
```xml
<?xml version="1.0" encoding="utf-8"?>
<TableLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="$media:beauty"
    ohos:column_count="3"
    ohos:padding="8vp"
    ohos:row_count="3">

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="1"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="2"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="3"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="4"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="5"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="6"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="7"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="8"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>

    <Text
        ohos:height="60vp"
        ohos:width="60vp"
        ohos:background_element="#00BFFF"
        ohos:margin="8vp"
        ohos:text="9"
        ohos:text_alignment="center"
        ohos:text_size="20fp"/>
</TableLayout>
```
## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/tableLayoutDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右；top：居上；bottom：居下；horizontal_center：水平居中；vertical_center：垂直居中|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)|
|ohos:row_count|行数，样例：ohos:row_count="3"|
|ohos:column_count|列数，样例：ohos:column_count="3"|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)