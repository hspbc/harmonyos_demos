# 什么是TimePicker
TimePicker是一种时间滚轮式选择器.常见效果如下：  
![](https://img-blog.csdnimg.cn/img_convert/ffd2e5f9012a67c73a5a2eec3621ee42.gif)

## 1.基本用法
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/70b478766a2d9abd177956871a9cfa68.png)
### 代码
```xml
<TimePicker
        ohos:height="match_content"
        ohos:width="match_parent"/>
```
## 2.设置背景
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/cfe751a231c55448f57e43242d5cc9e6.png)
### 代码
```xml
<TimePicker
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:background_element="#d3d7d4"/>
```
## 3.设置文字字体大小和颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/0454f2fee25dc0600312db31032060ff.png)
### 代码
```xml
 <TimePicker
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:normal_text_color="#000000"
        ohos:normal_text_size="18fp"
        ohos:selected_text_color="#007DFF"
        ohos:selected_text_size="18fp"/>
```
## 4.设置分割条颜色
### 效果图
![](https://img-blog.csdnimg.cn/img_convert/6198de01b92cc4fd35e33385772372d4.png)
### 代码
```xml
<TimePicker
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:bottom_line_element="#B15BFF"
        ohos:normal_text_color="#000000"
        ohos:normal_text_size="18fp"
        ohos:selected_text_color="#007DFF"
        ohos:selected_text_size="18fp"
        ohos:top_line_element="#FFAD86"/>
```
## 基础样例完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/timePickerDemo

# 常用属性说明
| 属性名 |用途  |
|--|--|
|ohos:width|设置控件宽度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:height|设置控件高度,可设置为:match_parent(和父控件一样),match_content(按照内容自动伸缩),设置固定值(如200vp)|
|ohos:layout_alignment|在父控件内对齐方式,可选值：left：居左；start：居左；center：居中；right：居右；end：居右|
|ohos:background_element|设置背景,可以是色值(如#FF0000)或图片等|
|ohos:visibility|可选值: visible(显示), invisible(隐藏,但是仍占据UI空间),hide(隐藏,且不占UI空间)
|ohos:normal_text_color|设置非选中文字颜色，样例：ohos:normal_text_color="#000000"|
|ohos:normal_text_size|设置非选中文字大小，样例：ohos:normal_text_size="30fp"|
|ohos:selected_text_color|设置选中文字颜色，样例：ohos:selected_text_color="#000000"|
|ohos:selected_text_size|设置选中文字大小，样例：ohos:selected_text_size="30fp"|
|ohos:top_line_element|设置上分割线颜色，样例：ohos:top_line_element="#000000"|
|ohos:bottom_line_element|设置下分割线颜色，样例：ohos:bottom_line_element="#000000"|
|ohos:wheel_mode_enabled|列表是否循环，可选值：true：循环；false：不循环，样例：ohos:wheel_mode_enabled="false"|
|ohos:shader_color|设置阴影颜色，样例：ohos:shader_color="#ECF5FF"|
|ohos:operated_text_color|设置当前被滚动的控件的选中文字颜色，样例：ohos:operated_text_color="#ECF5FF"|
更多属性及实际效果,可以在开发工具里自行体验.

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/3a1baf2918e8492ab61fa0600fb43444.png)