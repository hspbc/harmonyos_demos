package cn.hsp.harmonyosdemos.slice;

import cn.hsp.harmonyosdemos.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.components.TimePicker;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 厦门大学计算机专业 | 前华为工程师
 * 专注《零基础学编程系列》https://cxyxy.blog.csdn.net/article/details/121134634
 * 包含：Java | 安卓 | 前端 | Flutter | iOS | 小程序 | 鸿蒙
 * 公众号：花生皮编程
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

//        Text text_time = (Text) findComponentById(ResourceTable.Id_text_time);
//        TimePicker timePicker = (TimePicker) findComponentById(ResourceTable.Id_time_picker);
        // 获取时间
//        int hour = timePicker.getHour();
//        int minute = timePicker.getMinute();
//        int second = timePicker.getSecond();
//        //设置时间
//        timePicker.setHour(19);
//        timePicker.setMinute(18);
//        timePicker.setSecond(12);

//        timePicker.setTimeChangedListener((timePicker1, hour1, minute1, second1) -> text_time.setText("选择时间:"+ hour1 +"/"+ minute1 +"/"+ second1));

//        // 隐藏小时的显示
//        timePicker.showHour(false);
//        // 隐藏分钟
//        timePicker.showMinute(false);
//        // 隐藏秒
//        timePicker.showSecond(false);

//        // 设置小时selector无法滚动选择
//        timePicker.enableHour(false);
//        // 设置分钟selector无法滚动
//        timePicker.enableMinute(false);
//        // 设置秒selector无法滚动
//        timePicker.enableSecond(false);
    }
}