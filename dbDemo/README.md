# 什么是关系数据库
鸿蒙关系数据库是适合在移动设备上使用的轻量型关系型数据库,速度快,占用空间小.与轻量数据库适合存储少量简单类型数据相反,关系数据库适合存储大量复杂类型的数据.
# 基础样例
![](https://img-blog.csdnimg.cn/img_convert/88f8b76898887862387dc9e5ac6606aa.gif)
1. 拷贝数据库相关依赖jar到工程目录下
- 文件：orm_annotations_java.jar和orm_annotations_processor_java.jar
存放目录样例：D:\Program Files\Huawei\sdk\java\3.0.0.0\build-tools\lib
![](https://img-blog.csdnimg.cn/img_convert/ca1e534fa9c8e023d3c287fb7496d942.png)
- 拷贝到工程entry目录下libs里
![](https://img-blog.csdnimg.cn/img_convert/867b8385d29359713e3633d6bcd637ff.png)
2. 修改工程的build.gradle文件
添加如下行：
```
annotationProcessor files("./libs/orm_annotations_java.jar", "./libs/orm_annotations_processor_java.jar")
```
![](https://img-blog.csdnimg.cn/img_convert/b60a3f9762e88e5133653b3a7c11509e.png)
3. 添加数据库对象定义:OrmUser.java
```
@Entity(tableName = "OrmUser")
public class OrmUser extends OrmObject {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int userId;
    private String userName;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getUserId() {
        return userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
}
```
4. 添加数据库操作对象定义:OrmDBDemo.java
```
@Database(entities = {OrmUser.class}, version = 1)
public abstract class OrmDBDemo extends OrmDatabase {
}
```
5. 修改Slice代码：MainAbilitySlice.java
```java
public class MainAbilitySlice extends AbilitySlice {
    private OrmContext ormContext;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_writeText).setClickedListener(component -> write());
        findComponentById(ResourceTable.Id_readText).setClickedListener(component -> read());
        findComponentById(ResourceTable.Id_modifyText).setClickedListener(component -> modify());
        findComponentById(ResourceTable.Id_delText).setClickedListener(component -> del());

        initDb();
    }

    private void initDb() {
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        ormContext = databaseHelper.getOrmContext("OrmDBTest", "OrmDBTest.db", OrmDBDemo.class);
    }

    private void write() {
        int userId = 1;
        OrmUser ormUser = new OrmUser();
        ormUser.setUserId(userId);
        ormUser.setUserName("花生皮编程");
        ormContext.insert(ormUser);
        ormContext.flush();
    }

    private void read() {
        List<OrmUser> ormUsers = query();
        new ToastDialog(getContext()).setText(ormUsers.get(0).getUserName()).show();
    }

    private List<OrmUser> query() {
        //查询userId = 1的数据
        int userId = 1;
        OrmPredicates ormPredicates = ormContext.where(OrmUser.class).equalTo("userId", userId);
        return ormContext.query(ormPredicates);
    }

    private void modify() {
        //将查询出来的数据值修改后更新到数据库
        OrmUser ormUser = query().get(0);
        if (ormUser == null) {
            return;
        }
        ormUser.setUserName("花生皮編程2");
        ormContext.update(ormUser);
        ormContext.flush();
    }

    private void del() {
        //将查询出来的第一条数据从数据库中删除
        OrmUser ormUser = query().get(0);
        if (ormUser == null) {
            return;
        }
        ormContext.delete(ormUser);
        ormContext.flush();
    }
}
```
6. 对应页面布局文件:
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:orientation="vertical">

    <Text
        ohos:id="$+id:writeText"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="写数据"
        ohos:text_size="20fp"/>

    <Text
        ohos:id="$+id:readText"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="读数据"
        ohos:text_size="20fp"/>

    <Text
        ohos:id="$+id:modifyText"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="修改数据"
        ohos:text_size="20fp"/>

    <Text
        ohos:id="$+id:delText"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="删除数据"
        ohos:text_size="20fp"/>
</DirectionalLayout>
```

PS: 上述代码已包含数据库常见的增删改查功能，代码已优化至最简。

## 完整源代码
https://gitee.com/hspbc/harmonyos_demos/tree/master/dbDemo

# 关于我
厦门大学计算机专业 | 前华为工程师  
分享编程技术，没啥深度，但看得懂，适合初学者。  
Java | 安卓 | 前端 | 小程序 | 鸿蒙  
公众号：**花生皮编程**  
![](https://img-blog.csdnimg.cn/img_convert/123a1c3355c3ca04d3b88ab5e5648127.png)